package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class InterestActivity extends Activity implements OnClickListener {

	float interestRate;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_interest);
		
		Button b1 = (Button)findViewById(R.id.btnSUBMIT);
		b1.setOnClickListener(this);
		
		Button b2 = (Button)findViewById(R.id.btnSETTING);
		b2.setOnClickListener(this);
		
		TextView inRate = (TextView)findViewById(R.id.inRate);
		interestRate = Float.parseFloat(inRate.getText().toString());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.interest, menu);
		return true;
	}
	
	@Override
	public void onClick(View v) {
		int id = v.getId();
		
		if (id == R.id.btnSUBMIT) {
			EditText etinput1 = (EditText)findViewById(R.id.Input1);
			EditText etinput2 = (EditText)findViewById(R.id.Input2);		
			TextView output = (TextView)findViewById(R.id.Output);
			
			String res = "";
			try {
				double Input1 = Float.parseFloat(etinput1.getText().toString());
				double Input2 = Float.parseFloat(etinput2.getText().toString());
				double floatres = Input1*Math.pow(1+(interestRate/100), Input2);
				
				res = String.format(Locale.getDefault(), "%.2f", floatres);
			} catch(NumberFormatException e) {
				res = "Invalid input";
			} catch(NullPointerException e) {
				res = "Invalid input";
			}
			output.setText(res);
		}
		else if (id == R.id.btnSETTING) {
			Intent i = new Intent(this, SettingActivity.class);
			i.putExtra("interestRate", interestRate);
			startActivityForResult(i, 9999);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 9999 && resultCode == RESULT_OK) {
			interestRate = data.getFloatExtra("interestRate", 32.0f);
			TextView inRate = (TextView)findViewById(R.id.inRate);
			inRate.setText(String.format(Locale.getDefault(), "%.2f", interestRate));
		}
	}

	
}
