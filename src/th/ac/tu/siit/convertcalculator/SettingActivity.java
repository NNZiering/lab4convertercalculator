package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SettingActivity extends Activity implements OnClickListener {
	
	float exchangeRate;
	float interestRate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);
		
		Button b1 = (Button)findViewById(R.id.btnFinish);
		b1.setOnClickListener(this);
		
		Intent i = this.getIntent();
		exchangeRate = i.getFloatExtra("exchangeRate", 32.0f);
		interestRate = i.getFloatExtra("interestRate", 10.0f);
		
		EditText extRate = (EditText)findViewById(R.id.extRate);
		extRate.setText(String.format(Locale.getDefault(), "%.2f", exchangeRate));
		
		EditText intRate = (EditText)findViewById(R.id.intRate);
		intRate.setText(String.format(Locale.getDefault(), "%.2f", interestRate));
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.setting, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		EditText intRate = (EditText)findViewById(R.id.intRate);
		EditText extRate = (EditText)findViewById(R.id.extRate);
		try {
			float r = Float.parseFloat(extRate.getText().toString());
			float r2 = Float.parseFloat(intRate.getText().toString());
			//CREATE AN INTENT TO WRAP THE RETURN VALUE
			Intent data = new Intent();
			//ADD THE RETURN VALUE TO THE INTENT
			data.putExtra("exchangeRate", r);
			data.putExtra("interestRate", r2);
			//SET THE INTENT AS THE RESULT WHEN THIS ACTIVITY ENDS
			this.setResult(RESULT_OK, data);
			//END THIS ACTIVITY
			this.finish();
		} catch(NumberFormatException e) {
			Toast t = Toast.makeText(getApplicationContext(), 
					"Invalid exchange rate or interest rate", Toast.LENGTH_SHORT);
			t.show();
			extRate.setText("");
			intRate.setText("");
			extRate.requestFocus();
		} catch(NullPointerException e) {
			Toast t = Toast.makeText(getApplicationContext(), 
					"Invalid exchange rate or interest rate", Toast.LENGTH_SHORT);
			t.show();
			extRate.setText("");
			intRate.setText("");
			extRate.requestFocus();
		}
	}

}
